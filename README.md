# rsmt2 #

A generic library to interact with SMT lib 2 compliant solvers running in a separate system process, such as Z3 and CVC4.

* [documentation][doc]


# License

Copyright 2015 Adrien Champion. See the COPYRIGHT file at the top-level
directory of this distribution.

Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
http://www.apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
http://opensource.org/licenses/MIT>, at your option.


[doc]: http://adrienchampion.bitbucket.org/rsmt2/rsmt2/ (Documentation)